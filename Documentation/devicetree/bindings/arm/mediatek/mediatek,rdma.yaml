# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/arm/mediatek/mediatek,rdma.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: Mediatek Read Direct Memory Access

maintainers:
  - Matthias Brugger <matthias.bgg@gmail.com>

description: |
  Mediatek Read Direct Memory Access(RDMA) component used to do read DMA.
  It contains one line buffer to store the sufficient pixel data, and
  must be siblings to the central MMSYS_CONFIG node.
  For a description of the MMSYS_CONFIG binding, see
  Documentation/devicetree/bindings/arm/mediatek/mediatek,mmsys.yaml
  for details.
  The 1st RDMA is also used to be a controller node in Media Data Path 3(MDP3)
  that containing MMSYS, MUTEX, GCE and SCP settings.

properties:
  compatible:
    oneOf:
      - items:
          - const: mediatek,mt8183-mdp3
          - const: mediatek,mt8183-mdp3-rdma
      - items:
          - const: mediatek,mt8183-mdp3-rdma

  mediatek,scp:
    $ref: /schemas/types.yaml#/definitions/phandle
    maxItems: 1
    description: |
      The node of system control processor (SCP), using
      the remoteproc & rpmsg framework.
      $ref: /schemas/remoteproc/mtk,scp.yaml

  mediatek,mdp3-id:
    $ref: /schemas/types.yaml#/definitions/uint32
    maxItems: 1
    description: |
      There may be multiple blocks with the same function but
      different addresses in MDP3.
      In order to distinguish the connection with other blocks,
      a unique ID is needed to dynamically use one or more identical
      blocks to implement multiple pipelines.

  mdp3-comps:
    $ref: /schemas/types.yaml#/definitions/string-array
    items:
      - enum:
          # MDP direct-link input path selection, create a
          # component for path connectedness of HW pipe control
          - mediatek,mt8183-mdp3-dl1
      - enum:
          - mediatek,mt8183-mdp3-dl2
      - enum:
          # MDP direct-link output path selection, create a
          # component for path connectedness of HW pipe control
          - mediatek,mt8183-mdp3-path1
      - enum:
          - mediatek,mt8183-mdp3-path2
      - enum:
          # Input DMA of ISP PASS2 (DIP) module for raw image input
          - mediatek,mt8183-mdp3-imgi
      - enum:
          # Output DMA of ISP PASS2 (DIP) module for YUV image output
          - mediatek,mt8183-mdp3-exto
    description: |
      MTK sub-system of direct-link or DIP

  mdp3-comp-ids:
    maxItems: 1
    $ref: /schemas/types.yaml#/definitions/uint32-array
    description: |
      Pipeline ID of MDP sub-system.

  reg:
    minItems: 1
    maxItems: 5
    description: |
      1st: basic HW address
      2nd: mediatek,mt8183-mdp-dl1, mediatek,mt8183-mdp-dl2
      3rd: mediatek,mt8183-mdp-path1
      4th: mediatek,mt8183-mdp-path2
      5th: mediatek,mt8183-mdp3-imgi, mediatek,mt8183-mdp3-exto

  mediatek,gce-client-reg:
    $ref: /schemas/types.yaml#/definitions/phandle-array
    description: |
      The register of client driver can be configured by gce with 4 arguments
      defined in this property, such as phandle of gce, subsys id,
      register offset and size.
      Each GCE subsys id is mapping to a client defined in the header
      include/dt-bindings/gce/<chip>-gce.h.

  power-domains:
    maxItems: 1

  clocks:
    minItems: 1
    maxItems: 6
    description: |
      1st: RDMA0 clock
      2nd: RSZ1 clock
      3rd: direck-link TX clock in MDP side
      4th: direck-link RX clock in MDP side
      5th: direck-link TX clock in ISP side
      6th: direck-link RX clock in ISP side

  iommus:
    maxItems: 1

  mediatek,mmsys:
    $ref: /schemas/types.yaml#/definitions/phandle
    maxItems: 1
    description: |
      The node of mux(multiplexer) controller for HW connections.

  mediatek,mm-mutex:
    $ref: /schemas/types.yaml#/definitions/phandle
    maxItems: 1
    description: |
      The node of sof(start of frame) signal controller.

  mediatek,mailbox-gce:
    $ref: /schemas/types.yaml#/definitions/phandle
    description: |
      The node of global command engine (GCE), used to read/write
      registers with critical time limitation.

  mboxes:
    $ref: /schemas/types.yaml#/definitions/phandle-array

  gce-subsys:
    $ref: /schemas/types.yaml#/definitions/phandle-array
    description: |
      sub-system id corresponding to the global command engine (GCE)
      register address.

if:
  properties:
    compatible:
      contains:
        const: mediatek,mt8183-mdp3

then:
  required:
    - mediatek,scp
    - mediatek,mmsys
    - mediatek,mm-mutex
    - mediatek,mailbox-gce
    - mboxes
    - gce-subsys

required:
  - compatible
  - mediatek,mdp3-id
  - reg
  - clocks
  - mediatek,gce-client-reg

additionalProperties: false

examples:
  - |
    #include <dt-bindings/clock/mt8183-clk.h>
    #include <dt-bindings/gce/mt8183-gce.h>
    #include <dt-bindings/power/mt8183-power.h>
    #include <dt-bindings/memory/mt8183-larb-port.h>

    mdp3_rdma0: mdp3_rdma0@14001000 {
      compatible = "mediatek,mt8183-mdp3",
                   "mediatek,mt8183-mdp3-rdma";
      mediatek,scp = <&scp>;
      mediatek,mdp3-id = <0>;
      mdp3-comps = "mediatek,mt8183-mdp3-dl1", "mediatek,mt8183-mdp3-dl2",
                   "mediatek,mt8183-mdp3-path1", "mediatek,mt8183-mdp3-path2",
                   "mediatek,mt8183-mdp3-imgi", "mediatek,mt8183-mdp3-exto";
      mdp3-comp-ids = <0 1 0 1 0 1>;
      reg = <0x14001000 0x1000>,
            <0x14000000 0x1000>,
            <0x14005000 0x1000>,
            <0x14006000 0x1000>,
            <0x15020000 0x1000>;
      mediatek,gce-client-reg = <&gce SUBSYS_1400XXXX 0x1000 0x1000>,
                                <&gce SUBSYS_1400XXXX 0 0x1000>,
                                <&gce SUBSYS_1400XXXX 0x5000 0x1000>,
                                <&gce SUBSYS_1400XXXX 0x6000 0x1000>,
                                <&gce SUBSYS_1502XXXX 0 0x1000>;
      power-domains = <&spm MT8183_POWER_DOMAIN_DISP>;
      clocks = <&mmsys CLK_MM_MDP_RDMA0>,
               <&mmsys CLK_MM_MDP_RSZ1>,
               <&mmsys CLK_MM_MDP_DL_TXCK>,
               <&mmsys CLK_MM_MDP_DL_RX>,
               <&mmsys CLK_MM_IPU_DL_TXCK>,
               <&mmsys CLK_MM_IPU_DL_RX>;
      iommus = <&iommu>;
      mediatek,mmsys = <&mmsys>;
      mediatek,mm-mutex = <&mutex>;
      mediatek,mailbox-gce = <&gce>;
      mboxes = <&gce 20 CMDQ_THR_PRIO_LOWEST 0>,
               <&gce 21 CMDQ_THR_PRIO_LOWEST 0>,
               <&gce 22 CMDQ_THR_PRIO_LOWEST 0>,
               <&gce 23 CMDQ_THR_PRIO_LOWEST 0>;
      gce-subsys = <&gce 0x14000000 SUBSYS_1400XXXX>,
                   <&gce 0x14010000 SUBSYS_1401XXXX>,
                   <&gce 0x14020000 SUBSYS_1402XXXX>,
                   <&gce 0x15020000 SUBSYS_1502XXXX>;
    };